import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { LoginFormPage } from '../pages/login/login';
// import { AboutPage } from '../pages/about/about';
import { SQLite, SQLiteObject} from '@ionic-native/sqlite';
import { ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  // rootPage:any = LoginFormPage;
  // rootPage:any = TabsPage;
  rootPage: any;

  db: SQLiteObject;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private sqlite: SQLite, private toastController: ToastController, private storage: Storage) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleLightContent();
      splashScreen.hide();
      // this.deleteDatabase();
      this.storage.get('user_id').then((data) =>{
        if(data) {
          this.storage.set('user_id', data);
          this.rootPage = TabsPage;
        }
        else {
          this.createDatabase();
          this.rootPage = LoginFormPage;
        }

      });
      
    });
  }

  createDatabase() {
    this.sqlite.create({
      name: 'scome.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
        this.db = db;
        db.executeSql('CREATE TABLE IF NOT EXISTS drinks(drink_id INTEGER PRIMARY KEY, save_date DATETIME, drink_name TEXT, drink_volume INT, drink_brix INT, gramsOfSugar INT, user_id INT, day_save TEXT, month_save TEXT)', [])
        .then(res => this.displayToast('Successfully Created database and drinks table!'))
        .catch(e => this.displayToast(JSON.stringify(e)));

        db.executeSql('CREATE TABLE IF NOT EXISTS users(user_id INTEGER PRIMARY KEY, username TEXT, password TEXT, gender TEXT)',[])
        .then(res => this.displayToast('SQL Executed'))
        .catch(e => this.displayToast(JSON.stringify(e)));

        let data:Array<string> = ['galecortez', 'helloworld', 'F'];
        db.executeSql('INSERT INTO users(username, password, gender) VALUES(?, ?, ?)', data)
        .then(response => { 
          this.storage.set('user_id', response.insertId);
          console.log(response);
           },
          err => {
            this.displayToast(JSON.stringify(err));
          });
          let dateToday = new Date();
          let monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
          let dayNames = ["Sun","Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

          let drink1:Array<string> = ['Coke', '350', '5', '10', '1', dayNames[new Date().getDay()], monthNames[new Date().getMonth()]]
          db.executeSql('INSERT INTO drinks(save_date, drink_name, drink_volume, drink_brix, gramsOfSugar, user_id, day_save, month_save) VALUES(datetime("now", "localtime"), ?, ?, ?, ?, ?, ?, ?)', drink1)
          .then(res => this.displayToast('Successfully inserted first drink'),
          err => {this.displayToast(JSON.stringify(err)); console.log(err); 
          });

          let drink2:Array<string> = ['C2', '100', '30', '10', '1', dayNames[new Date().getDay() - 1], monthNames[new Date().getMonth()]]
          db.executeSql('INSERT INTO drinks(save_date, drink_name, drink_volume, drink_brix, gramsOfSugar, user_id, day_save, month_save) VALUES(datetime("now", "-1 day"), ?, ?, ?, ?, ?, ?, ?)', drink2)
          .then(res => this.displayToast('Successfully inserted first drink'),
          err => {
            // this.displayToast(JSON.stringify(err)); 
            console.log(err); 
          });

          let drink3:Array<string> = ['Smart C', '350', '2', '17', '1', dayNames[new Date().getDay() - 2], monthNames[new Date().getMonth()]]
          db.executeSql('INSERT INTO drinks(save_date, drink_name, drink_volume, drink_brix, gramsOfSugar, user_id, day_save, month_save) VALUES(datetime("now", "-2 days"), ?, ?, ?, ?, ?, ?, ?)', drink3)
          .then(res => this.displayToast('Successfully inserted first drink'),
          err => {
            // this.displayToast(JSON.stringify(err)); 
            console.log(err); 
          });

          let drink4:Array<string> = ['Capuccino', '200', '7', '15', '1', dayNames[new Date().getDay() -3], monthNames[new Date().getMonth()]]
          db.executeSql('INSERT INTO drinks(save_date, drink_name, drink_volume, drink_brix, gramsOfSugar, user_id, day_save, month_save) VALUES(datetime("now", "-3 days"), ?, ?, ?, ?, ?, ?, ?)', drink4)
          .then(res => this.displayToast('Successfully inserted first drink'),
          err => {
            // this.displayToast(JSON.stringify(err)); 
            console.log(err); 
          });
    }).catch(e => {
      this.displayToast(JSON.stringify(e));
      console.log(JSON.stringify(e));
    });
  }

  displayToast(toastMessage) {
    const toast = this.toastController.create({  
      message: toastMessage,
      position: 'bottom',
      duration: 2000
    });
    toast.present();
  }

  deleteDatabase() {
    this.sqlite.deleteDatabase({ name: 'scome.db', location: 'default'});
  }

}
