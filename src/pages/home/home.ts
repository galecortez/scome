import { Component, NgZone, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial';
import { Storage } from '@ionic/storage';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
// import { connect } from 'http2';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  private drinkDetails: FormGroup;

  sensorData:string = "0";
  drinkName: string;
  drinkVolume: number;
  switch:any;
  user: any;
  prevData:any;

  validation_messages: any;

  constructor(public navCtrl: NavController, public toastCtrl: ToastController, private sqlite: SQLite, public bluetoothSerial: BluetoothSerial, public storage:Storage, private ngZone: NgZone, private formBuilder: FormBuilder) {

    this.drinkDetails = this.formBuilder.group({
      drinkBrix: [''],
      drinkName: ['', Validators.compose([Validators.required, Validators.pattern('^(?=.*[a-zA-Z])[a-zA-Z0-9]+$')])],
      drinkVolume: ['', Validators.compose([Validators.required, Validators.min(1), Validators.max(5000)])]
    });

    this.validation_messages = {
      'drinkName': [
        { type: 'required', message: 'Name of the beverage is required.'},
        { type: 'pattern', message: 'Name of the beverage must only contain numbers and letters.' }
      ],
      'drinkVolume': [
        { type: 'required', message: 'Volume of beverage is required.'},
        { type: 'min', message: 'Volume should be greater than 0.'},
        { type: 'max', message: 'Volume should be less than 5000'}
      ]
    };
    this.getUserInfo();
  }

  getUserInfo() {
    this.storage.get('user_id').then((data) => {
        this.user = data;
        console.log(this.user);
    });
  }

  ionViewWillEnter() {
    this.getUserInfo();
  }

  saveDrinkRecord() {
    if(this.drinkName !== '' && this.drinkVolume !== 0) {
    let user:any;
    this.sqlite.create({
      name: 'scome.db',
      location: 'default'
    }).then((db: SQLiteObject) => {

        console.log(this.user);
        let grams =  this.drinkDetails.value.drinkVolume/parseInt(this.sensorData);
        if(grams == Infinity || grams == undefined) {
          grams = 0;
        }
        let days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
        let gramsString = grams.toFixed(2);
        db.executeSql('INSERT INTO drinks(save_date, drink_name, drink_volume, drink_brix, gramsOfSugar, user_id, day_save, month_save) VALUES(datetime("now", "localtime"), ?, ?, ?, ?, ?, ?, ?)', [this.drinkDetails.value.drinkName, this.drinkDetails.value.drinkVolume, this.sensorData, gramsString, this.user, days[new Date().getDay()], new Date().getMonth()])
        .then(res => {
          console.log(res);
          this.displayToast('Successfully recorded drink!');
          this.drinkDetails.reset();
          this.sensorData = '0';
        })
        .catch(e => this.displayToast(JSON.stringify(e)));
    });
    }

    else {
      this.displayToast('Please fill-out the Name and Volume input.');
    }
  }

  getData() {
    // this.sqlite.create({
    //   name: 'scome.db',
    //   location: 'default'
    // }).then((db: SQLiteObject) => {
    //     db.executeSql('CREATE TABLE IF NOT EXISTS drinks(drink_id INTEGER PRIMARY KEY, save_date DATETIME, drink_name TEXT, drink_volume INT, drink_brix INT)', [])
    //     .then(res => console.log('SQL Executed'))
    //     .catch(e => console.log(e));
    // });
    // db.executeSql('SELECT * FROM drinks ORDER BY drink_id DESC', {})
    // .then()
  }

  displayToast(toastMessage) {
    const toast = this.toastCtrl.create({  
      message: toastMessage,
      position: 'bottom',
      duration: 2000
    });
    toast.present();
  }

  deviceSwitch() {
    if(this.switch) {
      this.bluetoothCheckStatus();
    }
    else {
      // console.log('switch is off!');
      this.bluetoothSerial.disconnect().then(res => this.displayToast('Disconnected from device.'));
    }
  }
  bluetoothCheckStatus() {
    // if(this.isToggled)
    this.bluetoothSerial.isEnabled().then(res => {
        this.connectToDevice('98:D3:36:81:14:C6');
        // this.connectToDevice('00:18:E4:35:2D:1D'); // Noel's device
    }).catch(e => {
      this.bluetoothSerial.enable();
      // this.displayDeviceList();
      this.connectToDevice('98:D3:36:81:14:C6');
      // this.connectToDevice('00:18:E4:35:2D:1D'); // Noel's device
    });
    // this.connectToDevice('98:D3:36:81:14:C6');
    // this.connectToDevice();
  }

  connectToDevice(macAddress) {
    let con = this.bluetoothSerial.connect(macAddress).subscribe(resp => {
      this.bluetoothSerial.isConnected().then(success => {
          // console.log(success);
          this.displayToast(success);
      },
      error => this.displayToast("There was a problem establishing connection with the device. Please try again."));
      // this.receiveDataFromDevice();
      this.data();
    }, 
      err => {
      console.log(err);
      this.switch = false;
      this.displayToast(err);
      });
  }

  receiveDataFromDevice() {
    console.log('receiving ..');
      // this.bluetoothSerial.subscribeRawData().subscribe((response) => {
        // console.log(response);
          this.bluetoothSerial.read().then(data => {
            console.log(data);
            // if(data == ''){
            //   this.sensorData = '0';
            // }
            // else
            // {
              this.prevData = this.sensorData;
              data = data.trim();
              this.sensorData = data.substring(0, 2);

              if(this.sensorData == '0' || this.sensorData == '') {
                this.sensorData = this.prevData;
                // this.displayToast('Cannot read data greater than 15° brix and less than 1° brix!');
              }
            // }
            // if(/\s/.test(data)){
            // }
            // else {
              // this.generateChart();              
            // }
            this.bluetoothSerial.clear();
          }).catch(err => {
            console.log('error');
            this.displayToast(JSON.stringify(err))
          });
    // }, err => {
    //   console.log(err);
    // });
  }

  data() {
    setInterval(() => {
      this.actualReading();
    }, 500);
  }

  actualReading() {
    this.ngZone.run(() => {
      this.receiveDataFromDevice();
    });
  }

}
