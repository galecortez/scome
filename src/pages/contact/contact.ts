import { Component, ViewChild } from '@angular/core';
import { NavController, App } from 'ionic-angular';
import { Chart } from 'chart.js';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Storage } from '@ionic/storage';
import { LoginFormPage } from '../login/login';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  @ViewChild('barCanvas') barCanvas;
  @ViewChild('lineCanvas') lineCanvas;

  barChart: any;
  lineChart:any;

  user: any;
  user_gender: any;
  barchartData: any;
  lineChartData: any;
  user_name: string;

  max_reach:any = false;

  db: SQLiteObject;

  constructor(public navCtrl: NavController, private sqlite:SQLite, private storage: Storage, private app: App) {
        this.getUserInfo();
        // this.getWeekData();
  }

  ionViewWillEnter() {
      this.getUserInfo();
  }

  logoutUser() {
      this.storage.remove('user_id');
      this.app.getRootNav().setRoot(LoginFormPage);
  }

  getUserInfo() {
    this.storage.get('user_id').then((data) => {
        this.user = data;
        this.loadInfo();
    });
  }

  loadInfo() {
    this.sqlite.create({
        name: 'scome.db',
        location: 'default'
    }).then((db: SQLiteObject) => {
        this.db = db;

        this.db.executeSql('SELECT * FROM users WHERE user_id="' + this.user + '"', [])
        .then(response => {
            if(response.rows.length > 0) {
                this.user_gender = response.rows.item(0).gender;
                this.user_name = response.rows.item(0).username;
            }
            else {
                console.log('No record found!');
            }
        }, error => {
            console.log(error);
        });
        let days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
        let data = [0,0,0,0,0,0,0];
        let month = (new Date().getMonth() + 1 > 9)? (new Date().getMonth() + 1): ("0" + (new Date().getMonth() + 1))
        let dateDay = (new Date().getDate() > 9)? (new Date().getDate()): ("0" + new Date().getDate());
        let dateToday = new Date().getFullYear() + '-' + month + '-' + dateDay;
        let lastWeek = new Date().getFullYear() + '-' + month + '-' + ((new Date().getDate() - 7 > 9)? (new Date().getDate() - 7): ("0" + (new Date().getDate() - 7)));
        // console.log('SELECT SUM(gramsOfSugar) FROM drinks WHERE user_id="' + this.user + '" AND save_date >= ' + lastWeek + ' AND save_date <= ' + dateToday + ' GROUP BY day_save ORDER BY day_save DESC LIMIT 7')
        // console.log(lastWeek);
        this.db.executeSql('SELECT *, SUM(gramsOfSugar) as sugarSum FROM drinks WHERE user_id="' + this.user + '" AND save_date >= "' + lastWeek + '" GROUP BY day_save', [])
        // this.db.executeSql('SELECT * sugarSum FROM drinks WHERE user_id="' + this.user + '"', [])
        .then(response => {
            console.log(response.rows);
            if(response.rows.length > 0) {
                for(let i =0; i < response.rows.length; i++) {
                    console.log(response.rows.item(i).sugarSum);
                    console.log(response.rows.item(i).day_save + ' sugarsum: ' + response.rows.item(i).sugarSum);
                    switch(response.rows.item(i).day_save){
                        case "Sun": data[0] = response.rows.item(i).sugarSum; break;
                        case "Mon": data[1] = response.rows.item(i).sugarSum; break;
                        case "Tue": data[2] = response.rows.item(i).sugarSum; break;
                        case "Wed": data[3] = response.rows.item(i).sugarSum; break;
                        case "Thu": data[4] = response.rows.item(i).sugarSum; break;
                        case "Fri": data[5] = response.rows.item(i).sugarSum; break;
                        case "Sat": data[6] = response.rows.item(i).sugarSum; break;
                    }
                
                }
                console.log(data);
                this.barchartData = data;
                this.generateChart();
            }
        }, error => {
          console.log(error);
        });

        let initialLineData = [0,0,0,0,0,0,0,0,0,0,0,0];
        this.db.executeSql('SELECT *, SUM(gramsOfSugar) as sugarSum FROM drinks WHERE user_id="' + this.user + '" GROUP BY month_save ORDER BY month_save DESC', [])
        .then(response => {
            if(response.rows.length > 0) {
                for(let i =0; i < response.rows.length; i++) {
                    switch(response.rows.item(i).month_save){
                        case "Jan": initialLineData[0] = response.rows.item(i).sugarSum; break;
                        case "Feb": initialLineData[1] = response.rows.item(i).sugarSum; break;
                        case "Mar": initialLineData[2] = response.rows.item(i).sugarSum; break;
                        case "Apr": initialLineData[3] = response.rows.item(i).sugarSum; break;
                        case "May": initialLineData[4] = response.rows.item(i).sugarSum; break;
                        case "Jun": initialLineData[5] = response.rows.item(i).sugarSum; break;
                        case "Jul": initialLineData[6] = response.rows.item(i).sugarSum; break;
                        case "Aug": initialLineData[7] = response.rows.item(i).sugarSum; break;
                        case "Sep": initialLineData[8] = response.rows.item(i).sugarSum; break;
                        case "Oct": initialLineData[9] = response.rows.item(i).sugarSum; break;
                        case "Nov": initialLineData[10] = response.rows.item(i).sugarSum; break;
                        case "Dec": initialLineData[11] = response.rows.item(i).sugarSum; break;
                    }
                
                }

                this.lineChartData = initialLineData;
                this.generateLineChart();
            }
        }, error => {
            console.log(error)
        });

        this.db.executeSql('SELECT SUM(gramsOfSugar) as sugarSum from drinks WHERE save_date LIKE "%' + dateToday + '%" AND user_id="' +  this.user + '"', [])
        .then(resp => {
            if(resp.rows.length > 0) {
                let sugarIntake = resp.rows.item(0).sugarSum;
                console.log(sugarIntake);
                if(this.user_gender == 'F') {
                    if(sugarIntake > 25) {
                        this.max_reach = true;
                    }
                }
                else {
                    if(sugarIntake > 37.5) {
                        this.max_reach = true;
                    }
                }
            }
        }, err => {
            console.log(err);
        });
    });
  }

  getMonthData() {
    //   this.db.executeSql('SELECT * FROM drinks WHERE ')
  }

  ionViewDidLoad() {
    // this.generateChart();
    this.getUserInfo();
  }

  generateChart() {
    // console.log(this.barchartData);
    this.barChart = new Chart(this.barCanvas.nativeElement, {

      type: 'bar',
      data: {
          labels: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
          datasets: [{
              label: '',
              data: this.barchartData,
              backgroundColor: [
                  'rgba(255, 99, 132, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(255, 206, 86, 0.2)',
                  'rgba(75, 192, 192, 0.2)',
                  'rgba(153, 102, 255, 0.2)',
                  'rgba(255, 159, 64, 0.2)',
                  'rgba(255, 100, 64, 0.2)'
              ],
              borderColor: [
                  'rgba(255,99,132,1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(255, 206, 86, 1)',
                  'rgba(75, 192, 192, 1)',
                  'rgba(153, 102, 255, 1)',
                  'rgba(255, 159, 64, 1)',
                  'rgba(255, 100, 64, 1)'
              ],
              borderWidth: 1
          }]
      },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero:true
                  }
              }]
          },
          legend: {
            display: false
          }
      }

  });
  }

  generateLineChart() {
    this.lineChart = new Chart(this.lineCanvas.nativeElement, {

        type: 'line',
        data: {
            labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            datasets: [
                {
                    label: "",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "rgba(75,192,192,0.4)",
                    borderColor: "rgba(75,192,192,1)",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "rgba(75,192,192,1)",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgba(75,192,192,1)",
                    pointHoverBorderColor: "rgba(220,220,220,1)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: this.lineChartData,
                    spanGaps: false,
                }
            ]
        },
        options: {
            legend: {
                display: false
            }
        }
    
      });
  }
}
