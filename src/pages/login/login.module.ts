import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginFormPage } from './login';

@NgModule({
  declarations: [
    LoginFormPage,
  ],
  imports: [
    IonicPageModule.forChild(LoginFormPage),
  ],
})
export class LoginPageModule {}
