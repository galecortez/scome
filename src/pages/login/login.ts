import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TabsPage } from '../tabs/tabs';
import { ToastController } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Http, RequestMethod, Request } from '@angular/http';
import { Headers } from '@angular/http';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ForgotPasswordPage } from '../forgot-password/forgot-password';

@Component({
  selector: 'page-login-form',
  templateUrl: 'login.html',
})
export class LoginFormPage {

  private cred: FormGroup;

  // username:any;
  // password:any;
  db:SQLiteObject;
  
  http:Http;
  mailgunUrl: string;
  mailgunApiKey: string;
  constructor(public navCtrl: NavController, public storage: Storage, public toastController: ToastController, public sqlite: SQLite, http:Http, private formBuilder: FormBuilder) {

    this.cred = this.formBuilder.group({
      username: ['', Validators.compose([Validators.maxLength(35), Validators.required])],
      password: ['', Validators.compose([Validators.required])]
    });
    this.http = http;
    this.mailgunUrl = "https://app.mailgun.com/app/domains/sandbox41989f5190774f91aae95f2ab915caf4.mailgun.org";
    this.mailgunApiKey = window.btoa("api:b0576e3b0c8dd2f1e1a8ba2ff3107d88-1b65790d-c10b7cf5");
    // this.storage.get('tutorialShown').then((result) => {

    //   if(result) {
    //     this.navCtrl.push(TabsPage)
    //   } else {
    //     this.navCtrl.push(TutorialPage); 
    //     this.storage.set('tutorialShown', true);
    //   }
    // });
  }

  // ionViewDidLoad() {
  //   this.displayToast('ionViewDidLoad LoginFormPage');
  // }

  loginAttempt() {
    console.log(this.cred.value.username + " password: " + this.cred.value.password);
    this.sqlite.create({
      name: 'scome.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      db.executeSql('SELECT * FROM users WHERE username="' + this.cred.value.username + '" AND password="' + this.cred.value.password + '"', [])
      .then(res => {
        if(res.rows.length > 0)
        {
          this.storage.set('user_id', res.rows.item(0).user_id);
          this.displayToast('Successfully logged in!');
          this.navCtrl.setRoot(TabsPage);
        }
        else
        {
          // this.displayToast('Invalid username or password!');
        }
      })
      .catch(e => {
        this.displayToast(e);
        console.log(e); 
      });
    });
  }

  registerUser() {
    this.navCtrl.push(RegisterPage);
  }

  displayToast(toastMessage) {
    const toast = this.toastController.create({  
      message: toastMessage,
      position: 'bottom',
      duration: 2000
    });
    toast.present();
  }

  sendEmail(recipient: string = 'ghiecee@gmail.com', subject: string = 'SCOME Password', message: string = 'This is your password') {
    this.navCtrl.push(ForgotPasswordPage);
    // var requestHeaders = new Headers();
    //     requestHeaders.append("Authorization", "Basic " + btoa("api:b0576e3b0c8dd2f1e1a8ba2ff3107d88-1b65790d-c10b7cf5"));
    //     requestHeaders.append("Content-Type", "application/x-www-form-urlencoded");
    //     this.http.request(new Request({
    //         method: RequestMethod.Post,
    //         url: "https://api.mailgun.net/v3/sandbox41989f5190774f91aae95f2ab915caf4.mailgun.org/messages",
    //         body: "from=scomeapp@gmail.com&to=" + recipient + "&subject=" + subject + "&text=" + message,
    //         headers: requestHeaders
    //     }))
    //     .subscribe(success => {
    //         console.log("SUCCESS -> " + JSON.stringify(success));
    //         this.displayToast('Sent!');
    //     }, error => {
    //         console.log("ERROR -> " + JSON.stringify(error));
    //         this.displayToast("Failed to send your password. Please check you internet connection.");
    //     });
  }

  // ionViewCanEnter() {
  //   if(this.storage.get('user_id')) {
  //     return false;
  //   }
  // }
}