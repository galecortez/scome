import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http, RequestMethod, Request } from '@angular/http';
import { Headers } from '@angular/http';
import { ToastController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

/**
 * Generated class for the ForgotPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPasswordPage {

  emailPass: FormGroup;
  password: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private http: Http, private toastController: ToastController, private formBuilder: FormBuilder, private sqlite: SQLite) {
    this.emailPass = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      username: ['', Validators.required]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgotPasswordPage');
  }

  checkIfUserExist() {
    this.sqlite.create({
      name: 'scome.db',
      location: 'default'
  }).then((db: SQLiteObject) => {
      db.executeSql('SELECT * FROM users WHERE username="' + this.emailPass.value.username + '"', [])
      .then(response => {
          if(response.rows.length > 0) {
              this.password = response.rows.item(0).password;
              this.sendEmail();
          }
          else {
              console.log('No record found!');
              this.displayToast("User does not exist!");
          }
      }, error => {
          console.log(error);
      });
    });
  }

  sendEmail() {
    let message = "This is your password: " + this.password;
    var requestHeaders = new Headers();
        requestHeaders.append("Authorization", "Basic " + btoa("api:b0576e3b0c8dd2f1e1a8ba2ff3107d88-1b65790d-c10b7cf5"));
        requestHeaders.append("Content-Type", "application/x-www-form-urlencoded");
        this.http.request(new Request({
            method: RequestMethod.Post,
            url: "https://api.mailgun.net/v3/sandbox41989f5190774f91aae95f2ab915caf4.mailgun.org/messages",
            body: "from=scomeapp@gmail.com&to=" + this.emailPass.value.email + "&subject=" + "SCOME Password" + "&text=" + message,
            headers: requestHeaders
        }))
        .subscribe(success => {
            console.log("SUCCESS -> " + JSON.stringify(success));
            this.displayToast('Sent!');
        }, error => {
            console.log("ERROR -> " + JSON.stringify(error));
            this.displayToast("Failed to send your password. Please check you internet connection.");
        });
  }

  displayToast(toastMessage) {
    const toast = this.toastController.create({  
      message: toastMessage,
      position: 'bottom',
      duration: 2000
    });
    toast.present();
  }

}
