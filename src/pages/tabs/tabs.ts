import { Component } from '@angular/core';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { TutorialPage } from '../tutorial/tutorial';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = TutorialPage;
  tab3Root = ContactPage;
  constructor() {

  }

  ionViewCanEnter() {
     
  }
}
