import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import am4themes_dark from "@amcharts/amcharts4/themes/dark";
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
// import { LocalNotifications, LocalNotificationsOriginal } from '@ionic-native/local-notifications';

am4core.useTheme(am4themes_dark);
am4core.useTheme(am4themes_animated);

@Component({
  selector: 'page-tutorial',
  templateUrl: 'tutorial.html',
})
export class TutorialPage {

  user:any;
  drinksList = [];
  db: SQLiteObject;
  totalSugarIntake:any;
  user_gender: any;
  maxSugar:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private sqlite: SQLite, private toastController: ToastController, private storage: Storage) {
    // this.getUserInfo();
  }

  @ViewChild('sugarMeterCon') sugarMeterCon;
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad TutorialPage');
  }

  ionViewWillEnter() {
    this.getUserInfo();
  }

  getUserInfo() {
    this.storage.get('user_id').then((data) => {
        this.user = data;
        this.loadData();
    });
  }

  loadData() {
    this.sqlite.create({
      name: 'scome.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
        this.db = db;
        // let today = new Date();
        // let stringDate = today.getFullYear() + '-' + (((today.getMonth() + 1) > 9)? today.getMonth() + 1: "0" + (today.getMonth() + 1)) + '-' + (((today.getDate()) > 9)? today.getDate(): "0" + today.getDate());
        let month = (new Date().getMonth() + 1 > 9)? (new Date().getMonth() + 1): ("0" + (new Date().getMonth() + 1))
        let dateDay = (new Date().getDate() > 9)? (new Date().getDate()): ("0" + new Date().getDate());
        let dateToday = new Date().getFullYear() + '-' + month + '-' + dateDay;

        this.storage.get('user_id').then((data) => {
          this.user = data;
      });
        // this.db.executeSql('SELECT * FROM drinks WHERE user_id="' + this.user + '"', [])
        // this.db.executeSql('SELECT * FROM drinks',[])
        this.db.executeSql('SELECT * FROM drinks WHERE user_id="' + this.user + '" AND save_date LIKE "%' + dateToday + '%"', [])
        .then(response => {
          if(response.rows.length > 0) {
            this.drinksList = [];
            let rows = response.rows;
            for(let i = 0; i < rows.length; i++) {
              this.drinksList.push({
                name: rows.item(i).drink_name,
                gramsOfSugar: rows.item(i).gramsOfSugar,
                volume: rows.item(i).drink_volume,
                save_date: rows.item(i).save_date,
                userid: rows.item(i).user_id
              });
              console.log(rows.item(i));
            }
          }
          else {
            this.displayToast('No data found!');
            console.log(response);
          }
        }, error => {
          this.displayToast(JSON.stringify(error));
          console.log(error);
        });

        this.db.executeSql('SELECT * FROM users WHERE user_id="' + this.user + '"', [])
        .then(response => {
            if(response.rows.length > 0) {
                this.user_gender = response.rows.item(0).gender;
            }
            else {
                console.log('No record found!');
            }
        }, error => {
            console.log(error);
        });

        this.db.executeSql('SELECT SUM(gramsOfSugar) as sugarSum FROM drinks WHERE user_id="' + this.user + '" AND save_date LIKE "%' + dateToday + '%"', [])
        .then(response => {
          if(response.rows.length > 0) {
            this.totalSugarIntake = response.rows.item(0).sugarSum;
            // this.localNotifications.hasPermission().then(resp => {
            //   this.localNotifications.schedule({
            //     id: 1,
            //     text: 'You already consumed ' + this.totalSugarIntake + ' grams for this day!',
            //     title: 'SCOME'
            //   });
            // }, err => {
            //   this.localNotifications.requestPermission().then(resp => {
            //     this.displayToast(resp);
            //   }, err => {

            //   });
            // });
            if(this.user_gender == 'F') {
                this.maxSugar = 25;
                // if(sugarIntake > 25) {
                //     this.max_reach = true;
                // }
            }
            else {
                // if(sugarIntake > 37.5) {
                //     this.max_reach = true;
                // }
                this.maxSugar = 37.5;
            }

            this.sugarMeter();
        }
        }, error => {

        });
    });
  }

  ngAfterViewInit() {
    // this.sugarMeter();
    // this.getUserInfo();
  }

  sugarMeter() {
    let chart = am4core.create(this.sugarMeterCon.nativeElement, am4charts.GaugeChart);
    chart.hiddenState.properties.opacity = 0; // this makes initial fade in effect

    chart.innerRadius = -25;
    chart.align = "center";
    chart.marginBottom = 20;

    let axis = chart.xAxes.push(new am4charts.ValueAxis<am4charts.AxisRendererCircular>());
    axis.min = 0;
    axis.max = (this.totalSugarIntake > this.maxSugar)? this.totalSugarIntake: this.maxSugar;
    axis.strictMinMax = true;
    axis.renderer.grid.template.stroke = new am4core.InterfaceColorSet().getFor("background");
    axis.renderer.grid.template.strokeOpacity = 0.3;

    let colorSet = new am4core.ColorSet();

    let range0 = axis.axisRanges.create();
    range0.value = 0;
    range0.endValue = 10;
    range0.axisFill.fillOpacity = 1;
    range0.axisFill.fill = colorSet.getIndex(0);
    range0.axisFill.zIndex = - 1;

    let range1 = axis.axisRanges.create();
    range1.value = 10;
    range1.endValue = 20;
    range1.axisFill.fillOpacity = 1;
    range1.axisFill.fill = colorSet.getIndex(2);
    range1.axisFill.zIndex = -1;

    let range2 = axis.axisRanges.create();
    range2.value = 20;
    range2.endValue = this.maxSugar;
    range2.axisFill.fillOpacity = 1;
    range2.axisFill.fill = colorSet.getIndex(4);
    range2.axisFill.zIndex = -1;

    let minVal = this.maxSugar;
    let maxVal = this.totalSugarIntake;
    if(maxVal > minVal) {
      let range3 = axis.axisRanges.create();
      range3.value = minVal;
      range3.endValue = maxVal;
      range3.axisFill.fillOpacity = 1;
      range3.axisFill.fill = am4core.color("#f9365a");
      range3.axisFill.zIndex = -1;
    }
    let hand = chart.hands.push(new am4charts.ClockHand());

      // using chart.setTimeout method as the timeout will be disposed together with a chart
      chart.setTimeout(randomValue, 2000);
      let x = this.totalSugarIntake;
      function randomValue() {
          hand.showValue(x, 1000, am4core.ease.cubicOut);
        }
  }

  displayToast(toastMessage) {
    const toast = this.toastController.create({  
      message: toastMessage,
      position: 'bottom',
      duration: 2000
    });
    toast.present();
  }

}
