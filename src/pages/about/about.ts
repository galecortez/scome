import { Component, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Chart } from 'chart.js';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Storage } from '@ionic/storage';
import { ToastController } from 'ionic-angular';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  db: SQLiteObject;

  noTableData: boolean;
  user:any;
  drinksList = [
    // { name: 'Coke', gramsOfSugar: 30, volume: 300},
    // { name: 'Sprite', gramsOfSugar: 50, volume: 1500},
    // { name: 'Vitamilk', gramsOfSugar: 45, volume: 450}
  ];

  constructor(public navCtrl: NavController, private sqlite: SQLite, private storage: Storage, private toastController: ToastController) {
    this.getUserInfo();
    // this.loadDrinks();
   
  } 

  ionViewWillEnter() {
    this.getUserInfo();
    // this.loadDrinks();
  }

  ionViewDidLoad(){
   
    // this.getUserInfo();
    // this.showChart();
  }

  getUserInfo() {
    this.storage.get('user_id').then((data) => {
        this.user = data;
        this.loadDrinks();
    });
  }

  loadDrinks() {
    this.sqlite.create({
      name: 'scome.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
        this.db = db;
        // let today = new Date();
        // let stringDate = today.getFullYear() + '-' + (((today.getMonth() + 1) > 9)? today.getMonth() + 1: "0" + (today.getMonth() + 1)) + '-' + (((today.getDate()) > 9)? today.getDate(): "0" + today.getDate());
        let month = (new Date().getMonth() + 1 > 9)? (new Date().getMonth() + 1): ("0" + (new Date().getMonth() + 1))
        let dateDay = (new Date().getDate() > 9)? (new Date().getDate()): ("0" + new Date().getDate());
        let dateToday = new Date().getFullYear() + '-' + month + '-' + dateDay;

        this.storage.get('user_id').then((data) => {
          this.user = data;
      });
        console.log(this.user);
        // this.db.executeSql('SELECT * FROM drinks WHERE user_id="' + this.user + '"', [])
        // this.db.executeSql('SELECT * FROM drinks',[])
        this.db.executeSql('SELECT * FROM drinks WHERE user_id="' + this.user + '" AND save_date LIKE "%' + dateToday + '%"', [])
        .then(response => {
          if(response.rows.length > 0) {
            this.noTableData = false;
            this.drinksList = [];
            let rows = response.rows;
            for(let i = 0; i < rows.length; i++) {
              this.drinksList.push({
                name: rows.item(i).drink_name,
                gramsOfSugar: rows.item(i).gramsOfSugar,
                volume: rows.item(i).drink_volume,
                save_date: rows.item(i).save_date,
                userid: rows.item(i).user_id
              });
              console.log(rows.item(i));
            }
          }
          else {
            this.displayToast('No data found!');
            this.noTableData = true;
            console.log(response);
          }
        }, error => {
          this.displayToast(JSON.stringify(error));
          console.log(error);
        });
    });
  }

  // ionViewDidLoad() {
  //   this.getUserInfo();
  //   this.showChart();
  // }

  displayToast(toastMessage) {
    const toast = this.toastController.create({  
      message: toastMessage,
      position: 'bottom',
      duration: 2000
    });
    toast.present();
  }

}
