import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { ToastController} from 'ionic-angular';
import { LoginFormPage } from '../login/login';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  username:string;
  password:string;
  conf_pass:string;
  gender: string;

  registerForm: FormGroup;
  validation_messages: any;

  submitAttempt: boolean = false;
  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder, public sqlite: SQLite, public toastController: ToastController) {

    this.registerForm = formBuilder.group({
      username: ['', Validators.compose([Validators.maxLength(25), Validators.minLength(5),Validators.pattern('^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]+$'), Validators.required])],
      password: ['', Validators.compose([Validators.minLength(5), Validators.required])],
      conf_pass: ['', Validators.compose([Validators.minLength(5), Validators.required])],
      gender: ['', Validators.compose([Validators.required])]
    });

    this.validation_messages = {
      'username': [
        { type: 'required', message: 'Username is required.' },
        { type: 'minlength', message: 'Username must be at least 5 characters long.' },
        { type: 'maxlength', message: 'Username cannot be more than 25 characters long.' },
        { type: 'pattern', message: 'Your username must contain only numbers and letters.' }
      ],
      'password': [
        { type: 'required', message: 'Password is required.'},
        { type: 'minlength', message: 'Password must be at least 5 characters.'}
      ],
      'conf_pass': [
        { type: 'required', message: 'Confirm password is required.'},
        { type: 'minlength', message: 'Password must be at least 5 characters.'}
      ],
      'gender': [
        { type: 'required', message: 'Gender is required.'}
      ]
    };
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  registerUser() {
    if(this.registerForm.valid){
      if(this.registerForm.value.password === this.registerForm.value.conf_pass) {
        this.sqlite.create({ name: 'scome.db', location: 'default'})
        .then((db: SQLiteObject) => {
          let data:Array<string> = [this.registerForm.value.username, this.registerForm.value.password, this.registerForm.value.gender];
          db.executeSql('INSERT INTO users(username, password, gender) VALUES(?, ?, ?)', data)
          .then(response => { 
            this.displayToast('Successfully created account!');
            this.navCtrl.push(LoginFormPage);
            },
            err => {
              this.displayToast(JSON.stringify(err));
            });
        });
      }
      else {
        this.displayToast('Password doesn\'t match. Please try again!');
      }
    }
  }

  displayToast(toastMessage) {
    const toast = this.toastController.create({  
      message: toastMessage,
      position: 'bottom',
      duration: 2000
    });
    toast.present();
  }

}
